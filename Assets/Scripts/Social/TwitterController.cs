﻿using UnityEngine;
using System.Collections;

public class TwitterController : MonoBehaviour {

	private const string TWITTER_ADDRESS = "http://twitter.com/intent/tweet";
	private const string TWEET_LANGUAGE = "en"; 
	private const string GAME_URL = "http://www.eduware.com/devgames/games/intensecalm/IntenseCalm.html";
	
	public void ShareToTwitter (string textToDisplay)
	{
		Application.OpenURL(TWITTER_ADDRESS +
		                    "?text=" + WWW.EscapeURL(textToDisplay) + GAME_URL +
		                    "&amp;lang=" + WWW.EscapeURL(TWEET_LANGUAGE));
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
