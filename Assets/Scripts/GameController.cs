﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {
	#region Member Variables
	public GameState state;
	public PlayerController _pc;
	public WorldController _wc;
	public GUIController _gui;
	public Transform BGM;
	public bool DEBUG;
	public bool tutorialMode;
	SFX _sfx;

	public float timeCurrent;
	int lastSecond;
	int lastHalfSecond;
	public bool newLongestRun = false;

	public float scoreMultiplier = 1.0f;
	public int score;
	public bool newHighScore = false;

	public bool paused = false;
	#endregion

	// Use this for initialization
	void Start () {
		state = GameState.Start;
		_sfx = Camera.main.GetComponent<SFX>();
		BGM = Camera.main.transform.FindChild("BGM");
		_gui =  GameObject.Find ("GUI").GetComponent<GUIController>();

		_gui.ShowStartGUI();

		tutorialMode = PlayerPrefs.GetInt("tutorialCompleted", 0) == 0;
	}
	
	/// <summary>
	/// Updates the gameController
	/// </summary>
	void Update () {

		// Debug/Cheat commands
		if (DEBUG) {
			// F2 to game over
			if (Input.GetKey(KeyCode.F2)) {
				GameOver();
			}
			// Press R to restart
			if (Input.GetKeyDown(KeyCode.R)) {
				Application.LoadLevel("Main");
			}
		}

		if (state == GameState.Running) {
			_pc.anim.SetTrigger(AnimTriggers.StartRunning);

			if (!tutorialMode) {
				timeCurrent += Time.deltaTime;
				// update player's score
				UpdateScore();
			}

			// Press P to pause 
			if (Input.GetKeyDown (KeyCode.P)) {
				Pause ();
			}		
		}
		else if (state == GameState.Start) {
			_pc.anim.SetTrigger(AnimTriggers.StartIdle);

			if (Input.GetKeyDown(KeyCode.Space) 
			    || Input.GetMouseButtonDown(0)) {
				StartGame();
			}
		}
		else if (state == GameState.Over) {

		}
		else if (state == GameState.Paused) {
			// Press P to unpause
			if (Input.GetKeyDown (KeyCode.P)) {
				Pause ();
			}
		}
		else {
			// ???
		}
	}

	void FixedUpdate () {
	}

	public void StartGame() {
		if (state == GameState.Running)
			return;

		state = GameState.Running;
		_pc.anim.SetTrigger(AnimTriggers.StartRunning);
		//_pc.sprite.particleSystem.enableEmission = true;

		_gui.ShowGameGUI();
	}

	/// <summary>
	/// Ends the Game
	/// </summary>
	public void GameOver () {
		if (state == GameState.Running) {
			state = GameState.Over;

			CameraShake camShake = Camera.main.GetComponent<CameraShake>();
			camShake.shakeAmount = 0.7f;
			camShake.shake = 1;

			// now check for high score/longest run
			UpdateHighScore();
			UpdateLongestRun();
		}

		_gui.ShowGameOverGUI();
	}

	/// <summary>
	/// Updates the score.
	/// </summary>
	public void UpdateScore() {
		// add score every half second
		if ((int)(timeCurrent * 10) % 5 == 0
		    && (int)(timeCurrent * 10) != lastHalfSecond) {
			//print (timeCurrent * 10);
			score += (int)(scoreMultiplier * 25);
			lastHalfSecond = (int)(timeCurrent * 10);
		}
		// once per second
		if ((int)timeCurrent != lastSecond) {

			// update lastsecond
			lastSecond = (int)timeCurrent;

			// every 10 seconds
			if (lastSecond % 10 == 0) {

				print (lastSecond + " seconds! Speeding up!");
				_wc.SpeedUp();
				//_wc.worldSpeed *= 1.15f;
				scoreMultiplier *= 1.5f;
				_sfx.PlaySFX(SFXType.SpeedUp);
			}
		}
	}

	/// <summary>
	/// Updates the high score.
	/// </summary>
	public void UpdateHighScore() {
		int lastHighScore = PlayerPrefs.GetInt(Prefs.HighScore);

		if (lastHighScore < score) {
			print ("New high score!");
			PlayerPrefs.SetInt(Prefs.HighScore, score);
			newHighScore = true;
		} 
		else {
			newHighScore = false;
		}
	}

	/// <summary>
	/// Updates the longest run.
	/// </summary>
	public void UpdateLongestRun() {
		float lastLongestRun = PlayerPrefs.GetFloat(Prefs.LongestRun);

		/*print ("Last longest run: " + lastLongestRun);
		print ("This run: " + timeCurrent);*/
		
		if (lastLongestRun < timeCurrent) {
			print ("New longest run!");
			PlayerPrefs.SetFloat(Prefs.LongestRun, timeCurrent);
			newLongestRun = true;
		} 
		else {
			newLongestRun = false;
		}
	}

	/// <summary>
	/// Pauses the game
	/// </summary>
	public bool Pause() {
		// pause the game
		if (state != GameState.Paused) {
			state = GameState.Paused;
			BGM.audio.Pause ();
			return true;
		}
		// other, resume it
		else {
			state = GameState.Running;
			BGM.audio.volume = 1.0f;
			BGM.audio.Play ();
			return false;
		}
	}

	/// <summary>
	/// Restart the game.
	/// </summary>
	public void Restart() {
		Application.LoadLevel("Main");
	}
}
