﻿using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour {
	#region member variables
	public GameController _gc;
	public PlayerController _pc;
	public SFX _sfx;
	public float distanceToPlayer;
	public float startingDistance = 5.0f;
	public float fallBackTimeFactor = 1.0f;
	public float dropThreatCD = 1.0f;
	public float dropThreatTime;
	public bool droppingThreat;
	public float epsilon = .05f;
	public bool gettingCloser = false;
	public float baddieSmooth = 5.0f;
	bool adjustingHeight = false;
	#endregion

	/// <summary>
	/// Start this instance.
	/// </summary>
	void Start () {
		_gc = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
		//_pc = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
		_sfx = Camera.main.GetComponent<SFX>();
	}

	/// <summary>
	/// Update this instance.
	/// </summary>
	void Update () {
		Vector2 playerPos = _pc.sprite.position;
		Animator anim = GetComponent<Animator>();

		// Game Running
		if (_gc.state == GameState.Running) {
			// make sure animator is set to regular speed
			anim.speed = 1;

			// always keep the enemy on same Y level as player
			//transform.position = new Vector2(transform.position.x, playerPos.y);
			if (!adjustingHeight) {
				StartCoroutine(AdjustHeight());
			}

			// keep track of previous distance to player
			float distanceToPlayerLastFrame = distanceToPlayer;
			// update distance to player
			distanceToPlayer = Vector2.Distance (playerPos, transform.position);

			// if we just got closer to the player
			if (distanceToPlayerLastFrame - distanceToPlayer > 0) {
				gettingCloser = true;
				droppingThreat = false;
				// update our threat time
				dropThreatTime = Time.time + dropThreatCD;
			}
			else {
				gettingCloser = false;
			}

			// if we should be dropping threat 
			if (!droppingThreat && dropThreatTime < Time.time) {
				droppingThreat = true;

				float fallbackX = playerPos.x - startingDistance;

				//print ("Enemy is falling back!");
				StartCoroutine(FallBack(fallbackX));
			}

			/*if (gettingCloser && !gettingCloserLastFrame) {
				print ("getting closer!");
				_sfx.PlaySFX(SFXType.Enemy);
			}*/
		}
		// Paused
		else if (_gc.state == GameState.Paused) {
			anim.speed = 0;
		}
	}

	/// <summary>
	/// Lerps the enemy back to the startingDistance away from the player
	/// </summary>
	/// <param name="destX">Destination x.</param>
	IEnumerator FallBack(float destX) {

		while(Mathf.Abs(transform.position.y - destX) > epsilon)
		{
			if (_gc.state == GameState.Running) {
				transform.position = Vector3.Lerp(transform.position, // from
				                                  new Vector3(destX, transform.position.y, transform.position.z), // to
				                                  fallBackTimeFactor * Time.deltaTime); // time
				
			}

			yield return null;
		}
		//print ("Done falling back.");
		droppingThreat = false;
	}

	IEnumerator AdjustHeight ()
	{
		adjustingHeight = true;
		while(Mathf.Abs(transform.position.y - _pc.sprite.transform.position.y) > epsilon)
		{
			transform.position = Vector3.Lerp(transform.position, 
			                                  new Vector3(transform.position.x, _pc.sprite.transform.position.y, transform.position.z), 
			                                  baddieSmooth * Time.deltaTime);
			
			yield return null;
		}
		adjustingHeight = false;
	}

	void OnTriggerEnter2D (Collider2D other) {;
		if (other.tag == "Player") {
			//print ("PLAYER GOT TOUCHED");
			PlayerController pc = other.transform.parent.GetComponent<PlayerController>();
			
			Explode();
			pc.Die ();
		}
	}

	void Explode() {
		Animator anim = GetComponent<Animator>();

		anim.SetTrigger("startExplosion");
	}

	void LevelWithplayer() {

	}
}
