﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof (GUITexture))]

public class Button : MonoBehaviour {

	//GUIController _gui;

	public Texture2D buttonUpTexture;
	public Texture2D buttonDownTexture;
	
	void Start() {
		//_gui = GameObject.Find ("GUI").GetComponent<GUIController>();
	}
	
	void OnMouseOver() {
		guiTexture.texture = buttonDownTexture;
	}
	
	void OnMouseExit() {
		guiTexture.texture = buttonUpTexture;
	}
	
	void OnMouseDown() {
		guiTexture.texture = buttonDownTexture;
	}

	void OnMouseUpAsButton() {
		guiTexture.texture = buttonUpTexture;
		ButtonPressed();
	}

	public virtual void ButtonPressed() {}
}
