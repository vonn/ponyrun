﻿using UnityEngine;
using System.Collections;

public class TweetBtn : Button {

	public TwitterController twitter;

	public int score;
	public string runTime;

	void Start () {	}

	public override void ButtonPressed() {
		twitter.ShareToTwitter("I got " + score + " points on a " + runTime + " run in " +
		                       "Intense Calm: Story of Spring! ");
	}
}
