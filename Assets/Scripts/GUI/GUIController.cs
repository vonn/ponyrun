using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class GUIController : MonoBehaviour {

	#region Member Variables
	public GameController _gc;

	// Buttons
	public GameObject pauseBtn;
	public GameObject replayBtn;
	public GameObject tweetBtn;
	public GameObject menuBtn;

	// Misc
	public GameObject darkOverlay;

	// GUI holders
	public GameObject startGUI;
	public GameObject gameGUI;
	public GameObject pauseGUI;
	public GameObject gameOverGUI;

	// gameoverGUI stuff
	float flashTime1;
	float flashTime2;
	public float flashCD = .03f;
	#endregion

	#region Unity Methods
	// Use this for initialization
	void Start () {
	}

	void Update() {
	}

	void FixedUpdate() {
		if (_gc.state == GameState.Running) {
			UpdateInGameLabels();
		}
		else if (_gc.state == GameState.Over) {
			FlashNewThings();
		}
	}
	#endregion

	#region Show/Hide sections
	/* ~ START ~ */
	public void ShowStartGUI() {
		HideAllGUI();
		darkOverlay.SetActive(true);
		startGUI.SetActive(true);
	}

	void HideStartGUI() {
		darkOverlay.SetActive(false);
		startGUI.SetActive(false);
	}
	
	/* ~ IN GAME ~ */
	public void ShowGameGUI() {
		HideAllGUI();
		pauseBtn.SetActive(true);
		gameGUI.SetActive(true);
	}

	void HideGameGUI() {
		pauseBtn.SetActive(false);
		gameGUI.SetActive(false);
	}
	
	/* ~ PAUSE ~ */
	public void ShowPauseGUI() {
		HideAllGUI();
		darkOverlay.SetActive(true);
		pauseBtn.SetActive(true);
		pauseGUI.SetActive(true);
	}

	void HidePauseGUI() {
		darkOverlay.SetActive(false);
		pauseBtn.SetActive(false);
		pauseGUI.SetActive(false);
	}
	
	/* ~ GAME OVER ~ */
	public void ShowGameOverGUI() {
		HideAllGUI();
		UpdateGameOverLabels();
		SendInfoToTwitterBtn();
		darkOverlay.SetActive(true);
		gameOverGUI.SetActive(true);
		replayBtn.SetActive(true);
		tweetBtn.SetActive(true);
		menuBtn.SetActive(true);
	}

	void HideGameOverGUI() {
		darkOverlay.SetActive(false);
		gameOverGUI.SetActive(false);
		replayBtn.SetActive(false);
		tweetBtn.SetActive(false);
		menuBtn.SetActive(false);
	}
	
	/* ~ MISC. ~ */
	void HideAllGUI() {
		HideGameGUI();
		HideGameOverGUI();
		HidePauseGUI();
		HideStartGUI();
	}
	#endregion

	#region GUI Button Functions
	/// <summary>
	/// when pause button is pressed
	/// </summary>
	public void PauseButtonPressed() {
		// Game Paused
		if (_gc.Pause ()) {
			ShowPauseGUI();
		}
		// Unpaused
		else {
			ShowGameGUI();
		}
	}

	/// <summary>
	/// When replay button is pressed
	/// </summary>
	public void ReplayButtonPressed() {
		if (_gc.state != GameState.Over) return;

		_gc.Restart();
	}
	#endregion

	#region Aux Functions
	void UpdateGameOverLabels() {
		// first grab all the labels
		GUIText yourScoreLabel = gameOverGUI.transform.FindChild("yourScoreLabel").GetComponent<GUIText>();
		GUIText yourTimeLabel = gameOverGUI.transform.FindChild("yourTimeLabel").GetComponent<GUIText>();
		GUIText highScoreLabel = gameOverGUI.transform.FindChild("highScoreLabel").GetComponent<GUIText>();
		GUIText highTimeLabel = gameOverGUI.transform.FindChild("highTimeLabel").GetComponent<GUIText>();

		// aaaand what should be going in there
		string yourScore = _gc.score + "";
		string time = Utils.TimeToStr(_gc.timeCurrent);
		string highScore = PlayerPrefs.GetInt(Prefs.HighScore) + "";
		string highTime = Utils.TimeToStr(PlayerPrefs.GetFloat(Prefs.LongestRun));

		// now assign the text to labels
		yourScoreLabel.text = yourScore;
		yourTimeLabel.text = time;
		highScoreLabel.text = highScore;
		highTimeLabel.text = highTime;
	}

	void UpdateInGameLabels() {
		// grabs the labels
		GUIText timeLabel = gameGUI.transform.FindChild("timeLabel").GetComponent<GUIText>();
		GUIText scoreLabel = gameGUI.transform.FindChild("scoreLabel").GetComponent<GUIText>();
		//GUIText multLabel = gameGUI.transform.FindChild("multLabel").GetComponent<GUIText>();

		// set up what should go in
		string time = Utils.TimeToStr(_gc.timeCurrent);
		string score = _gc.score + "";
		//string mult = _gc.scoreMultiplier.ToString("0.00") + "x";

		// and put it in
		timeLabel.text = "Time: " + time;
		scoreLabel.text = /*"Score: " +*/ score;
		//multLabel.text = "Multiplier: " + mult;
	}

	void FlashNewThings() {
		// grab the labels
		GUIText newHighScoreLabel = gameOverGUI.transform.FindChild("newHighScoreLabel").GetComponent<GUIText>();
		GUIText newHighTimeLabel = gameOverGUI.transform.FindChild("newHighTimeLabel").GetComponent<GUIText>();
		GUIText highScoreLabel = gameOverGUI.transform.FindChild("highScoreLabel").GetComponent<GUIText>();
		GUIText highTimeLabel = gameOverGUI.transform.FindChild("highTimeLabel").GetComponent<GUIText>();

		Color yellowish = new Color(255, 195, 0, 255);
		Color invis = new Color(255, 255, 255, 0);

		if (_gc.newHighScore && flashTime1 < Time.time) {
			// toggle opacity of "NEW!" by high score
			if (newHighScoreLabel.color.a >= 200)
				newHighScoreLabel.color = invis;
			else
				newHighScoreLabel.color = yellowish;

			// toggle opacity of high score
			if (highScoreLabel.color.a >= 200)
				highScoreLabel.color = invis;
			else
				highScoreLabel.color = yellowish;

			flashTime1 = Time.time + flashCD;
		}
		else {
			newHighScoreLabel.color = invis;
		}

		if (_gc.newLongestRun && flashTime2 < Time.time) {
			// toggle opacity of "NEW!" by longest time
			if (newHighTimeLabel.color.a >= 200)
				newHighTimeLabel.color = invis;
			else
				newHighTimeLabel.color = yellowish;

			// toggle opacity of "NEW!" by longest time
			if (highTimeLabel.color.a >= 200)
				highTimeLabel.color = invis;
			else
				highTimeLabel.color = yellowish;

			flashTime2 = Time.time + flashCD;
		}
		else {
			newHighTimeLabel.color = invis;
		}
	}

	void SendInfoToTwitterBtn() {
		TweetBtn btn = tweetBtn.GetComponent<TweetBtn>();
		btn.score = _gc.score;
		btn.runTime = Utils.TimeToStr(_gc.timeCurrent);
	}
	#endregion
}
