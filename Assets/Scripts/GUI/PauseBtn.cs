﻿using UnityEngine;
using System.Collections;

public class PauseBtn : MonoBehaviour {

	GameController _gc;
	GUIController _gui;

	public Texture2D pauseButtonUp;
	public Texture2D pauseButtonDown;
	public Texture2D playButtonUp;
	public Texture2D playButtonDown;

	void Start() {
		_gc = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
		_gui = GameObject.Find ("GUI").GetComponent<GUIController>();
	}

	void OnMouseOver() {
		// Pause Btn
		if (_gc.state == GameState.Running) {
			guiTexture.texture = pauseButtonDown;
		}
		// Play Btn
		else if (_gc.state == GameState.Paused) {
			guiTexture.texture = playButtonDown;
		}
	}

	void OnMouseExit() {
		// Pause Btn
		if (_gc.state == GameState.Running) {
			guiTexture.texture = pauseButtonUp;
		}
		// Play Btn
		else if (_gc.state == GameState.Paused) {
			guiTexture.texture = playButtonUp;
		}
	}

	void OnMouseDown() {
		// Pause Btn
		if (_gc.state == GameState.Running) {
			guiTexture.texture = pauseButtonDown;
		}
		// Play Btn
		else if (_gc.state == GameState.Paused) {
			guiTexture.texture = playButtonDown;
		}
	}

	void OnMouseUpAsButton() {
		// Pause Btn
		if (_gc.state == GameState.Running) {
			guiTexture.texture = pauseButtonUp;
		}
		// Play Btn
		else if (_gc.state == GameState.Paused) {
			guiTexture.texture = playButtonUp;
		}
		_gui.PauseButtonPressed();
	}
}
