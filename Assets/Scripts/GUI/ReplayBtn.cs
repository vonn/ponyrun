﻿using UnityEngine;
using System.Collections;

public class ReplayBtn : Button {

	GUIController _gui;

	void Start() {
		_gui = GameObject.Find ("GUI").GetComponent<GUIController>();
	}

	public override void ButtonPressed() {
		_gui.ReplayButtonPressed();
	}
}
