﻿using UnityEngine;
using System.Collections;

public class QuitBtn : Button {

	public override void ButtonPressed() {
		Application.Quit();
	}
}
