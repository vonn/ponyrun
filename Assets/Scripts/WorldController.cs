﻿using UnityEngine;
using System.Collections;

public class WorldController : MonoBehaviour {
	#region Member variables
	public PlayerController _pc;
	public GameController _gc;

	public Vector2 worldSpeed = new Vector2(.25f, 0);
	public float worldAccel = .0009f;
	Vector2 desiredSpeed;
	float speedIncreaseFactor = 1.15f;

	public Transform _platforms;
	public Transform tutorialPrefab;
	public Platform[] platformPrefabs;
	public Pickup[] pickupPrefabs;
	
	public Transform _spawner;
	public bool lastSpawnedPlatform;
	public float lastSpawnedPlatformHeight;
	public float focusHeight = -1;
	public int focusCtr;
	public float spawnerCD = .4f;
	public float nextSpawnTime;
	public Transform _bgBack;
	public Transform _bgMid;
	public float parallaxFactor = .015f;
	//public bool newSpeed = false;
	#endregion

	void Start () {
		_platforms = transform.FindChild("platforms");
		// create obj pools
		for (int i = 1; i < platformPrefabs.Length; i++) {
			ObjectPool.CreatePool(platformPrefabs[i]);
		}
		for (int i = 1; i < pickupPrefabs.Length; i++) {
			ObjectPool.CreatePool(pickupPrefabs[i]);
		}
		
		_bgBack = transform.FindChild("bg back");
		_bgMid = transform.FindChild("bg mid");

		//_spawner = transform.FindChild("spawner");
		// disable the spawner's renderer
		//_spawner.renderer.enabled = false;
	}

	// Update is called once per frame
	void Update () {
		bool justSpawnedAPlatform = false;

		// while game is running
		if (_gc.state == GameState.Running) {

			// MOVE ALL THE PLATFORMS
			for (int i = 0; i < _platforms.childCount; i++) {
				Transform thisPlat = _platforms.GetChild(i);
				// move platform
				thisPlat.position += (Vector3)(-worldSpeed);
			}

			// Spawn Platforms
			if (!CheckForPlatforms() && nextSpawnTime < Time.time) {
				if (!lastSpawnedPlatform) {
					// spawn platform
					SpawnPlatform(false);
					justSpawnedAPlatform = true;
					lastSpawnedPlatform = true;
				}
				else {
					// maybe spawn platform
					if (SpawnPlatform(true)) {
						lastSpawnedPlatform = true;
						justSpawnedAPlatform = true;
					}
					else {
						lastSpawnedPlatform = false;
					}
				}
				nextSpawnTime = Time.time + spawnerCD;
			}

			// Which platform am I focused on?
			if (justSpawnedAPlatform) {
				focusCtr--;
				// update focus height
				if (focusCtr <= 0) {
					focusHeight = lastSpawnedPlatformHeight;
					focusCtr = 3;	// update focus every 4 platforms
				}
			}

			// Update the parallax bg
			ParallaxBG();
		}
	}

	/// <summary>
	/// Update per frame
	/// </summary>
	void FixedUpdate() {
		// Accelerate world to desired speed
		if (worldSpeed.x < desiredSpeed.x) {
			worldSpeed.x += worldAccel;
		}
	}

	/// <summary>
	/// Checks for platforms under the spawner.
	/// </summary>
	/// <returns><c>true</c>, if platform found, <c>false</c> otherwise.</returns>
	bool CheckForPlatforms() {
		RaycastHit2D hit = Physics2D.Raycast(_spawner.position, -Vector2.up, 200, Layers2D.terrainOnlyLayerMask);

		if (hit) {
			// got a hit!
			Debug.DrawRay(_spawner.position, -Vector2.up*200, Color.green);
			return true;
		}
		else {
			// no platform here
			Debug.DrawRay(_spawner.position, -Vector2.up*200, Color.red);
			return false;
		}
	}

	/// <summary>
	/// Spawns the platform.
	/// </summary>
	/// <returns><c>true</c>, if platform was spawned, <c>false</c> otherwise.</returns>
	public bool SpawnPlatform(bool allowNoPlatform) {
		// pick a random platform index
		int index = Random.Range (0, allowNoPlatform ? platformPrefabs.Length + 1 : platformPrefabs.Length);

		// didn't spawn a platform
		if (index > platformPrefabs.Length-1) {
			
			//SpawnGem ();

			return false;
		}

		// hacky fix
		if (lastSpawnedPlatformHeight < 0) lastSpawnedPlatformHeight = _pc.sprite.position.y;


		// Spawn normally
		if (!_gc.tutorialMode) {
			float rand = Random.Range (-3.0f, 3.0f);

			// sign flip maybe
			if (Random.Range(0, 100) % 2 == 0) rand *= -1.0f;

			// create new height
			float newHeight = lastSpawnedPlatformHeight + rand;

			// floor or ceil it if it's too low (prevent underflow?)
			if (newHeight < .01f) {
				newHeight = Random.Range(0, 100) % 2 == 0 ? 0 : 1;
				//print ("new height very small!");
			}

			Platform newPlat = ObjectPool.Spawn (platformPrefabs[index], new Vector3(_spawner.position.x, newHeight));
			newPlat.transform.parent = _platforms;

		}
		// Spawn the tutorial if they haven't completed it
		else {
			Transform tutorialObj = ObjectPool.Spawn (tutorialPrefab, new Vector3(_spawner.position.x, -3.848134f));
			tutorialObj.transform.parent = _platforms;

			lastSpawnedPlatformHeight = 6.0f;
		}
		return true;
	}

	/// <summary>
	/// Spawns a gem.
	/// </summary>
	void SpawnGem() {
		float gemHeight = lastSpawnedPlatformHeight + 2.0f;
		int pickupIndex = Random.Range(0, pickupPrefabs.Length);
		Pickup newPickup = ObjectPool.Spawn (pickupPrefabs[pickupIndex], new Vector3(_spawner.position.x, gemHeight));
		newPickup.transform.parent = _platforms;
	}

	/// <summary>
	/// Speeds up the world to the new value.
	/// </summary>
	public void SpeedUp() {
		desiredSpeed = worldSpeed;
		desiredSpeed *= speedIncreaseFactor;
	}

	/// <summary>
	/// Controls parallax bg
	/// </summary>
	void ParallaxBG() {

		// Back
		float offsetFactorBack = worldSpeed.x * parallaxFactor * .92f;
		_bgBack.renderer.material.mainTextureOffset +=  new Vector2(offsetFactorBack, 0);

		// just keep it the same Y as the cam
		_bgBack.position = new Vector3(_bgBack.position.x, Camera.main.transform.position.y, _bgBack.position.z);

		// Mid
		float offsetFactorMid = worldSpeed.x * parallaxFactor * 1.08f;
		_bgMid.renderer.material.mainTextureOffset +=  new Vector2(offsetFactorMid, 0);
		
		// just keep it the same Y as the cam
		_bgMid.position = new Vector3(_bgMid.position.x, Camera.main.transform.position.y - 2.9f, _bgMid.position.z);
	}
}
