﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Parallaxes Backgrounds based on their Z-distance from the camera.
/// </summary>
/// <description>
/// Author: Kenneth Ramos (gjvonn@gmail.com)
/// Date: July 2014
/// 
/// Instructions: 
/// - Place this script on an empty gameobject whose children are textured quads.
/// - Set the Shader on the quad's material to Unlit/Shader
/// - The further from the camera (z-distance) the child is, the slower it will scroll
/// 
/// NOTE: This script tries to keep the backgrounds on the same Y as the camera (no vertical parallax)
/// </description>
public class BackgroundController : MonoBehaviour {

	#region Member Vars
	// private
	public float _speed = .05f;
	#endregion

	#region Unity methods
	void Update () {
		// Where's the camera?
		float _camZ = Camera.main.transform.position.z;

		// Apply parallax to each child
		for (int i = 0; i < transform.childCount; i++) {

			Transform currentBG = transform.GetChild(i);
			float distFromCam = currentBG.position.z - _camZ;

			// you'll never see this, but keeps bgs that are behind the camera scrolling in same direction
			//if (currentBG.position.z < _camZ) distFromCam *= -1;

			ParallaxBG(currentBG, distFromCam);
		}
	}
	#endregion

	/// <summary>
	/// Offsets the texture on a bg by it's dist
	/// </summary>
	/// <param name="bg">Background.</param>
	/// <param name="dist">Dist.</param>
	void ParallaxBG(Transform bg, float dist) {
		// Don't divide by 0 pls.
		if (dist == 0) return; 

		float offsetFactor = _speed / dist;
		bg.renderer.material.mainTextureOffset +=  new Vector2(offsetFactor, 0);
		
		// just keep it the same Y relative to the camera
		float relativeY = Camera.main.transform.position.y - bg.position.y;

		bg.position = new Vector3(bg.position.x, Camera.main.transform.position.y - relativeY, bg.position.z);
	}
}
