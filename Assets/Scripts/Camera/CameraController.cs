﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

	public PlayerController _pc;
	public WorldController _wc;
	public GameController _gc;
	public float threshold = 1.0f;
	public float padding = .6f;
	public float camSmooth = 1.0f;
	public float playerOffset = 4.5f;

	float playerY;
	float focusY;
	float diff;	
	float midPt;
	float destY;

	bool movingCam;

	void Start () {

	}

	void Update () {
		
		if (_gc.state != GameState.Running) return;

		// height diff between player and last platform spawned
		playerY = _pc.sprite.position.y;
		focusY = _wc.focusHeight;
		diff = focusY - playerY;	// negative diff -> 
		midPt = diff/2 + playerY;
		padding = focusY < playerY ? -padding : padding;
		destY = midPt + padding;


		DebugLines();

		if (!movingCam && _pc.grounded) {

			//print ("player = " +  playerY + ", lastHeight = " + focusY + ", diff = " + diff + ", midPt = " + midPt);
			//print ("Moving cam");
			movingCam = true;
			//StartCoroutine( AdjustCam( midPt /*+ (playerY < focusY ? padding : -padding)*/ ) );
			
			StartCoroutine( AdjustCam( midPt ) );
		}

		transform.position = new Vector3(_pc.sprite.transform.position.x+playerOffset,
		                                 transform.position.y, transform.position.z);
	}

	IEnumerator AdjustCam (float newY)
	{
		while(Mathf.Abs(transform.position.y - newY) > 0.05f)
		{
			transform.position = Vector3.Lerp(transform.position, 
			                                  new Vector3(transform.position.x, newY, transform.position.z), 
			                                  camSmooth * Time.deltaTime);
			
			yield return null;
		}
		//print ("Done moving cam.");
		movingCam = false;
	}

	// draw crazy rays
	void DebugLines() {
		float x = 20;
		// focusY line
		Debug.DrawLine(new Vector2(-x, focusY), new Vector2(x, focusY), Color.red);

		// midpoint line
		Debug.DrawLine(new Vector2(-x, midPt), new Vector2(x, midPt), Color.yellow);

		// player line
		Debug.DrawLine(new Vector2(-x, playerY), new Vector2(x, playerY), Color.green);

		// destination for camera
		Debug.DrawLine(new Vector2(-x, destY), new Vector2(x, destY), Color.cyan);
	}
}
