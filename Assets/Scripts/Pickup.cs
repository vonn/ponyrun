﻿using UnityEngine;
using System.Collections;

public class Pickup : MonoBehaviour {

	public int points;
	GameController _gc;
	
	void OnEnable()
	{
		_gc = GameObject.Find("GameController").GetComponent<GameController>();
	}
	
	void OnDisable()
	{
		StopAllCoroutines();
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.tag == "Player") {
			PlayerController pc = other.transform.parent.GetComponent<PlayerController>();
			
			Bloom();
			pc.PickUp(points);


			//this.Recycle ();
		}
	}

	void FixedUpdate() {
		
		if (transform.position.x < -10 * _gc.transform.position.x) {
			this.Recycle();
		}
	}

	/// <summary>
	/// Bloom this instance.
	/// </summary>
	void Bloom() {
		Animator anim = GetComponent<Animator>();
		anim.SetTrigger("bloom");

		ParticleSystem particle = GetComponent<ParticleSystem>();
		particle.Play();
		particle.Emit (10);
	}
}
