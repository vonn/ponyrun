﻿using UnityEngine;
using System.Collections;
using System;

public static class Utils {

	/// <summary>
	/// Gets the bounds of the sprite in world coords
	/// </summary>
	/// <returns>Vector3[2] with top-left and bottom-right of sprite in world coords</returns>
	/// <param name="tr">The Transform component with a SpriteRenderer.</param>
	public static Vector3[] SpriteLocalToWorld(Transform tr) 
	{
		Sprite sp = tr.GetComponent<SpriteRenderer>().sprite;
		Vector3 pos = tr.position;
		Vector3 [] array = new Vector3[2];
		//top left
		array[0] = pos + sp.bounds.min;
		// Bottom right
		array[1] = pos + sp.bounds.max;
		return array;
	}

	/// <summary>
	/// Time float to string.
	/// </summary>
	/// <returns>The time as string.</returns>
	/// <param name="time">Time.</param>
	public static string TimeToStr(float time) {
		TimeSpan t = TimeSpan.FromMilliseconds(time * 1000);
		string answer = string.Format("{0:D2}:{1:D2}:{2:D2}", 
		                              t.Minutes, 
		                              t.Seconds, 
		                              t.Milliseconds);
		return answer;
	}
}
