﻿using UnityEngine;
using System.Collections;

public class TutorialTrigger : MonoBehaviour {

	GameController _gc;

	void Start() {
		_gc = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
	}

	void OnTriggerEnter2D (Collider2D col) {
		if (col.tag == "Player") {
			print ("TUTORIAL COMPLETE");
			_gc.tutorialMode = false;
			PlayerPrefs.SetInt("tutorialCompleted", 1);
		}
	}
}
