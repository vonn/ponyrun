﻿using UnityEngine;
using System.Collections;

public class MusicManager : MonoBehaviour {

	// musics
	public AudioClip MenuMusic;
	public AudioClip GameMusic;
	// singleton business
	private static MusicManager instance = null;
	public static MusicManager Instance {
		get { return instance; }
	}

	void Awake() {
		if (instance != null && instance != this) {
			Destroy(this.gameObject);
			return;
		} else {
			instance = this;
		}
		DontDestroyOnLoad(this.gameObject);

		PlayMenuMusic();
	}
	
	// any other methods you need

	// Menu music
	public void PlayMenuMusic() {
		audio.Stop();
		audio.clip = MenuMusic;
		audio.Play ();
	}

	// game music
	public void PlayGameMusic() {
		audio.Stop();
		audio.clip = GameMusic;
		audio.Play ();
	}

	public void StopMusic() {
		audio.Stop ();
	}
}
