﻿using UnityEngine;
using System.Collections;

public enum SFXType
{
	Jump,
	Land,
	Hurt,
	Explode,
	Enemy,
	Pickup,
	SpeedUp
}

/// <summary>
/// Players sound effects from the player's audio source based on the provided SFXType enum.
/// </summary>
public class SFX : MonoBehaviour {
	
	public AudioClip jump;
	public AudioClip land;
	public AudioClip hurt;
	public AudioClip explode;
	public AudioClip enemy;
	public AudioClip pickup;
	public AudioClip speedUp;
	
	public float masterVolume = 0.4f;
	
	
	public void PlaySFX(SFXType someSFX)
	{
		
		audio.volume = masterVolume;
		audio.loop = false;
		//audio.Stop();
		switch(someSFX)
		{
		case SFXType.Jump:
			audio.PlayOneShot(jump);
			break;
		case SFXType.Land:
			audio.PlayOneShot(land);
			break;
		case SFXType.Hurt:
			audio.PlayOneShot(hurt);
			break;
		case SFXType.Explode:
			audio.PlayOneShot(explode);
			break;
		case SFXType.Enemy:
			audio.PlayOneShot(enemy);
			break;
		case SFXType.Pickup:
			audio.PlayOneShot(pickup);
			break;
		case SFXType.SpeedUp:
			audio.PlayOneShot(speedUp);
			break;
		}
	}
}
