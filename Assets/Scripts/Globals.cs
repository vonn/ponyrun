﻿using UnityEngine;
using System.Collections;

public enum GameState { Start, Running, Over, Paused };

public enum PlayerState { Alive, Dead };

public class Anims {
	public const string Idle = "idle";
	public const string Running = "run";
	public const string StartJump = "jump_start";
	public const string Airborne = "jump_airborne";
	public const string Landing = "jump_land";
}

public class AnimTriggers {
	public const string StartIdle = "startIdle";
	public const string StartRunning = "startRunning";
	public const string StartJumping = "startJumping";
	public const string StartLanding = "startLanding";
	public const string StartSmoke = "startSmoke";
}

public class Prefs {
	public const string HighScore = "highScore";
	public const string LongestRun = "longestRun";
}

public class Layers2D
{
	public const int platformingLayerMask = (1 << Layers2D.terrainLayer) 	|
											(1 << Layers2D.playerLayer)		|
											(1 << Layers2D.miscLayer);
	
	public const int terrainOnlyLayerMask = (1 << Layers2D.terrainLayer);
	
	public const int playerRaycastLayer = 	(1 << Layers2D.terrainLayer)	|
											(1 << Layers2D.hazardLayer)     |
											(1 << Layers2D.miscLayer);
	
	public const int defaultLayer = 0;
	public const int TransparentFXLayer = 1;
	public const int IgnoreRaycastLayer = 2;
	public const int terrainLayer = 8;
	public const int playerLayer = 9;
	public const int ignorePlayerLayer = 12;
	public const int hazardLayer = 15;
	public const int deadLayer = 16;
	public const int miscLayer = 17;
	public const int boundaryLayer = 20;
}