﻿using UnityEngine;
using System.Collections;

public class Platform : MonoBehaviour {

	GameController _gc;
	
	void OnEnable()
	{
		_gc = GameObject.Find("GameController").GetComponent<GameController>();
	}
	
	void OnDisable()
	{
		StopAllCoroutines();
	}
	
	void FixedUpdate() {

		if (transform.position.x < -10 * _gc.transform.position.x) {
			this.Recycle();
		}
	}
}
