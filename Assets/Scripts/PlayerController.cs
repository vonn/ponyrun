﻿
using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {
	#region Member Variables
	public GameController _gc;
	public WorldController _wc;
	public float gravity = .017f;
	public float jumpForce = .25f;
	public float jumpAccel = .025f;
	public float jumpSpeedCap = .29f;
	public float jumpCD = .4f;
	public float jumpForgiveCD = .1f;
	public float jumpForgiveTime;
	public float frontCheckDist = .64f;
	public float groundCheckDist = .64f;
	public bool grounded;
	public bool groundedLastFrame;
	public bool inAir;
	public bool inAirLastFrame;
	public bool canHighJump = true;
	public bool jumpNextChance = false;
	public bool isInvincible = false;
	public float invincibilityTime;
	public float invincibilityCD = 1.0f;
	public Vector2 vel;
	public Transform sprite;
	public Transform fx;
	public Vector2 fxOffset = new Vector2(-.05f, -.4f);
	[HideInInspector]
	public Animator anim;
	[HideInInspector]
	public Animator fxAnim;
	float jumpTime;
	float terminalVelocity = -1.0f;
	SFX _sfx;
	#endregion


	// Use this for initialization
	void Start () {
		anim = sprite.GetComponent<Animator>();
		_wc = GameObject.Find ("World").GetComponent<WorldController>();
		_sfx = Camera.main.GetComponent<SFX>();
		fxAnim = fx.GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		// DEBUG / cheat commands
		if (_gc.DEBUG) {
			// F1 to throw the player in the middle
			if (Input.GetKey(KeyCode.F1)) {
				sprite.position = new Vector2(sprite.position.x, 0);
				vel.y = 0;
			}
			// F3 to become a trigger
			if (Input.GetKeyDown(KeyCode.F3)) {
				// make this a trigger so it goes through shit
				sprite.GetComponent<BoxCollider2D>().isTrigger = true;
			} else if (Input.GetKeyUp(KeyCode.F3)){				
				sprite.GetComponent<BoxCollider2D>().isTrigger = false;
			}
		}

		// Pause animation
		if (_gc.state == GameState.Paused) {
			anim.speed = 0;
		}
		else if (_gc.state == GameState.Running) {
			float factor = _wc.worldSpeed.x / .13f; // i found .13 is where animSpeed = 1 looks nice
			anim.speed = 1 * factor ;
		}

		// Game not running?
		if (_gc.state != GameState.Running) return;

		#region Input
		// Tap jump on ground (Normal jump)
		if ( (grounded ||  jumpForgiveTime < Time.time)
		      && (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0)) ) {
			Jump ();
		}
		// Tap jump in air (
		else if ((Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0))) {
			TryJumpNextChance();
		}
		// Release Jump (can't high jump)
		else if ( canHighJump && (Input.GetKeyUp(KeyCode.Space) || Input.GetMouseButtonUp(0)) ) {
			canHighJump = false;
		}
		#endregion

		// GROUNDED CHECKING
		if (GroundCheck() && jumpTime < Time.time) {
			vel.y = 0;
			// ground me!
			if (!groundedLastFrame) {
				anim.SetTrigger (AnimTriggers.StartLanding);
				grounded = true;

				_sfx.PlaySFX(SFXType.Land);

				// maybe i can jump?
				if (jumpNextChance) {
					Jump ();
				}
			}
		}
		// no ground beneath
		else {
			vel.y -= gravity;
			grounded = false;
			jumpForgiveTime = Time.time + jumpForgiveCD;
		}

		// FRONT COLLISION CHECK
		// Just hit a wall?
		if (FrontCheck () && !isInvincible && invincibilityTime < Time.time) {
			//print ("Ouchies @ " + Time.time);
			Ouch();
		}
		// time's up for invincibility
		else if ( isInvincible && invincibilityTime < Time.time) {
			isInvincible = false;
			sprite.GetComponent<SpriteRenderer>().color = Color.white;
		}
		// while i'm invincible though
		else if (isInvincible) {
			sprite.GetComponent<SpriteRenderer>().color = Color.red;
		}

		// always line with sprite

	}
	
	void FixedUpdate () {
		if (_gc.state != GameState.Running) return;

		// update lastframe bools
		groundedLastFrame = grounded;

		Vector2 pos = sprite.transform.position;

		#region Input
		// Hold Jump (high jump)
		if ( canHighJump && (Input.GetKey(KeyCode.Space) || Input.GetMouseButton(0)) ) {
			HighJump ();
		}
		else {
		}
		#endregion
		
		// Apply the changes
		sprite.transform.position = pos + vel;

		// DEATH CONDITION
		if (vel.y <= terminalVelocity) Die ();
	}

	/// <summary>
	/// What happens when I die?
	/// </summary>
	public void Die () {
		print ("DEAD");
		
		// make this a trigger so it goes through shit
		sprite.GetComponent<BoxCollider2D>().isTrigger = true;
		// and give it a gravity scale
		sprite.rigidbody2D.gravityScale = 5.0f;

		//sfx
		_sfx.PlaySFX(SFXType.Explode);

		_gc.GameOver();
	}

	/// <summary>
	/// Hops. Affects vel.x and vel.y
	/// </summary>
	void Jump() {
		//print("Jumping");

		jumpNextChance = false;
		Vector2 spritePos = sprite.position;

		if (_gc.state != GameState.Running || !grounded)
			return;

		jumpTime = Time.time + jumpCD;
		vel.y = vel.y < 0 ? jumpForce : vel.y + jumpForce;
		canHighJump = true;
		anim.SetTrigger (AnimTriggers.StartJumping);
		
		// make sure fx anim is where horse jumped
		fx.position = (Vector3)(spritePos + fxOffset);
		fxAnim.SetTrigger(AnimTriggers.StartSmoke);

		_sfx.PlaySFX(SFXType.Jump);
	}

	/// <summary>
	/// That Mario jump when you hold down for longer
	/// </summary>
	public void HighJump() {

		if (!canHighJump)
			return;

		if (vel.y < jumpSpeedCap ){
			vel.y += jumpAccel;
		}
		else {
			canHighJump = false;
		}
	}

	/// <summary>
	/// Checks if the player is near/on the ground
	/// </summary>
	/// <returns><c>true</c>, if so <c>false</c> otherwise.</returns>
	public bool GroundCheck() {

		Vector3[] sprBounds = Utils.SpriteLocalToWorld(sprite);
		float leftPt = sprBounds[0].x;
		float rightPt = sprBounds[1].x;

		float[] pts = { leftPt+.3f, sprite.transform.position.x, rightPt-.1f };

		bool anyHit = false;

		// cast down 3 rays. if any hit, return true
		for (int i = 0; i < 3; i++) {
			Vector3 origin = new Vector2(pts[i], sprite.transform.position.y);
			
			Debug.DrawRay(origin, -Vector2.up*groundCheckDist, Color.blue);

			if (Physics2D.Raycast(origin, -Vector2.up, groundCheckDist, Layers2D.terrainOnlyLayerMask)) {
				
				Debug.DrawRay(origin, -Vector2.up*groundCheckDist, Color.red);
				anyHit = true;
			}
		}

		return anyHit;
	}

	/// <summary>
	/// If I'm close enough to the ground, jump on my next chance
	/// </summary>
	void TryJumpNextChance() {

		// only if player is falling
		if (vel.y > -0.005f) return;

		// ground detection a lil further
		//Ray2D ray = new Ray2D(sprite.transform.position, -Vector2.up);
		Vector3[] sprBounds = Utils.SpriteLocalToWorld(sprite);
		float leftPt = sprBounds[0].x;
		float rightPt = sprBounds[1].x;
		
		float[] pts = { leftPt+.3f, sprite.transform.position.x, rightPt-.1f };

		for (int i = 0; i < pts.Length; i++) {
			
			Vector3 origin = new Vector2(pts[i], sprite.transform.position.y);

			if (Physics2D.Raycast(origin, -Vector2.up, groundCheckDist * 4, Layers2D.terrainOnlyLayerMask)) {
				
				Debug.DrawRay(origin, -Vector2.up * groundCheckDist * 4, Color.red);
				jumpNextChance = true;
			}
			else {
				Debug.DrawRay(origin, -Vector2.up * groundCheckDist * 4, Color.cyan);
				jumpNextChance = false;
			}
		}
	}

	/// <summary>
	/// Checks to see if the player has collisions in the front
	/// </summary>
	/// <returns><c>true</c>, if check was fronted, <c>false</c> otherwise.</returns>
	public bool FrontCheck() {
		Vector3[] sprBounds = Utils.SpriteLocalToWorld(sprite);
		float topPt = sprBounds[1].y;
		float botPt = sprBounds[0].y;
		
		float[] pts = { topPt - .4f, sprite.transform.position.y, botPt };
		
		bool anyHit = false;
		
		// cast right 3 rays. if any hit, return true
		for (int i = 0; i < 2; i++) {
			Vector3 origin = new Vector2(sprite.transform.position.x + .4f, pts[i]);
			
			Debug.DrawRay(origin, Vector2.right*frontCheckDist, Color.blue);
			
			if (Physics2D.Raycast(origin, Vector2.right, frontCheckDist, Layers2D.terrainOnlyLayerMask)) {
				
				Debug.DrawRay(origin, Vector2.right*frontCheckDist, Color.red);
				anyHit = true;
			}
		}
		
		return anyHit;
	}

	/// <summary>
	/// When the player is hurtin'
	/// </summary>
	void Ouch() {
		// make my invincible for a bit
		isInvincible = true;
		invincibilityTime = Time.time + invincibilityCD;

		// shake cam
		CameraShake camShake = Camera.main.GetComponent<CameraShake>();
		camShake.shakeAmount = .4f;
		camShake.shake = .3f;

		// sfx
		_sfx.PlaySFX(SFXType.Hurt);

		anim.SetTrigger("startOuch");
	}

	/// <summary>
	/// The players picks up a thing worth points
	/// </summary>
	/// <param name="points">Points to add to score.</param>
	public void PickUp(int points) {
		_sfx.PlaySFX(SFXType.Pickup);
		_gc.score += (int)(_gc.scoreMultiplier * points);
	}
}
